"""
Constants for link shortener.
"""
from enum import Enum

SHORT_URL_LENGTH = 5


class OverloadedGsiKeys(Enum):
    """
    Represents the various pieces used to compose keys within the overloaded GSI pattern.
    """
    PK = 'PK'
    SK = 'SK'
    GSI_PREFIX = 'GSI'
    DELIMITER = '_'
