"""
UseCases for each action on a Repository we want to model.
"""
from ckf_api_toolkit.core import UseCase
from models.link import Link
from models.person_click import PersonClick


class CreateLinkUseCase(UseCase):
    link: Link
    name = "Create a link"

    def __init__(self, link: Link):
        self.link = link


class UpdateLinkUseCase(UseCase):
    link: Link
    name = "Update a link"

    def __init__(self, link: Link):
        self.link = link


class GetActualLinkFromShortLinkUseCase(UseCase):
    name = "Get actual URL from short link"
    link_id: str

    def __init__(self, link_id):
        self.link_id = link_id


class DeleteLinkUseCase(UseCase):
    name = "Delete a link"
    link_id: str

    def __init__(self, link_id):
        self.link_id = link_id


class CreatePersonClickUseCase(UseCase):
    person_click: PersonClick
    name = "Log a click from a person"

    def __init__(self, person_click: PersonClick):
        self.person_click = person_click


class GetTotalClicksUseCase(UseCase):
    link_id: str
    name = "Get total clicks for a link"

    def __init__(self, link_id: str):
        self.link_id = link_id


class GetTotalClicksByDayUseCase(UseCase):
    link_id: str
    date: str
    name = "Get total clicks for a given day for a link"

    def __init__(self, link_id: str, date: str):
        self.link_id = link_id
        self.date = date


class GetTotalClicksByPersonUseCase(UseCase):
    person_id: str
    link_id: str
    name = "Get total clicks for a given person for a link"

    def __init__(self, link_id: str, person_id: str):
        self.link_id = link_id
        self.person_id = person_id


class GetTotalPersonClicksByDayUseCase(UseCase):
    link_id: str
    person_id: str
    date: str
    name = "Get total clicks for a given person on a given day for a link"

    def __init__(self, link_id: str, person_id: str, date: str):
        self.link_id = link_id
        self.person_id = person_id
        self.date = date
