"""This script contains the Lambda handlers - entry points for the AWS Lambda service.
We instantiate the Actor with a DynamoDB client, and the link shortener Repository we created elsewhere.
Each handler function represents one Use Case from the Repository.
"""
from functools import wraps
from os import environ
from typing import Callable

from ckf_api_toolkit.aws_dynamo import DynamoClient
from ckf_api_toolkit.core import Actor
from ckf_api_toolkit.tools.api_response_handler import ApiResponseFactory, ResponseCode, Header
from ckf_api_toolkit.tools.api_response_handler.aws_api_gateway import AwsApiGatewayResponseFactory
from ckf_api_toolkit.tools.api_response_handler.aws_lambda import get_aws_lambda_path_param_arg_mapper, \
    aws_lambda_body_arg_mapper
from ckf_api_toolkit.tools.handler import handler
from ckf_api_toolkit.tools.logger import Logger, LogLevel

from models.link import Link
from models.person_click import PersonClick
from repository import LinkShortenerRepository, AlreadyExists

from use_cases import CreateLinkUseCase, UpdateLinkUseCase, GetActualLinkFromShortLinkUseCase, \
    CreatePersonClickUseCase, GetTotalClicksUseCase, GetTotalClicksByDayUseCase, GetTotalClicksByPersonUseCase, \
    GetTotalPersonClicksByDayUseCase

actor = Actor(DynamoClient(environ['AWS_REGION']), LinkShortenerRepository())

Logger(LogLevel.debug)


def retry_create_on_collision(func: Callable):
    """Decorator that will retry calling a function when an AlreadyExists exception is thrown

    Args:
        func (Callable): The function to decorate.

    Returns:
        The wrapper function.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except AlreadyExists:
            return wrapper(*args, **kwargs)

    return wrapper


@handler(AwsApiGatewayResponseFactory(return_trace=True), arg_mappers=[aws_lambda_body_arg_mapper])
@retry_create_on_collision
def create_link(response: ApiResponseFactory, body: {}) -> ApiResponseFactory:
    """Create a new Link item from front-end data.

    Args:
        response (ApiResponseFactory): Factory response object
        body (Dict): The event body from Lambda

    Returns:
        response: The response for this request from the actor
    """
    link = Link.creation_init_from_front_end(body)
    response.set_response_from_actor(
        actor,
        CreateLinkUseCase(link),
        'link'
    )
    return response


@handler(AwsApiGatewayResponseFactory(), arg_mappers=[get_aws_lambda_path_param_arg_mapper('id'), PersonClick.aws_lambda_person_id_mapper])
def get_actual_url_from_short(response: ApiResponseFactory, link_id: str, person_id: str) -> ApiResponseFactory:
    """Get the actual URL from a shortened link, and create the PersonClick for this click event. Return the HTTP code for
    a redirect along with the location header.

    Args:
        response (ApiResponseFactory): Factory response object
        link_id (str): ID of the Link item to retrieve
        person_id (str): ID of the Person for this click event(currently an IP address)

    Returns:
        response: The response for this request
    """
    link: Link = actor.run_use_case(GetActualLinkFromShortLinkUseCase(link_id))

    person_click = PersonClick.creation_init_by_click(link.link_id, person_id)
    actor.run_use_case(CreatePersonClickUseCase(person_click))

    response.set_success(GetActualLinkFromShortLinkUseCase.name, response_code=ResponseCode.REDIRECT_TEMP)
    response.add_to_response_data(link.actual_url, 'actualUrl')
    response.add_header(Header('Location', link.actual_url))
    return response


@handler(AwsApiGatewayResponseFactory(return_trace=True), arg_mappers=[get_aws_lambda_path_param_arg_mapper('id'), aws_lambda_body_arg_mapper])
def update_link(response: ApiResponseFactory, link_id: str, body: {}) -> ApiResponseFactory:
    """Update a Link item with a different actual URL.

    Args:
        response (ApiResponseFactory): Factory response object
        link_id (str): ID of the Link item to update
        body (Dict): The event body from Lambda

    Returns:
        response: The response for this request from the actor
    """
    link = Link.init_from_front_end(body)
    link.link_id = link_id
    response.set_response_from_actor(
        actor,
        UpdateLinkUseCase(link),
        'link'
    )
    return response


@handler(AwsApiGatewayResponseFactory(return_trace=True), arg_mappers=[get_aws_lambda_path_param_arg_mapper('id')])
def get_total_clicks_for_link(response: ApiResponseFactory, link_id: str) -> ApiResponseFactory:
    """Return the total number of clicks for a Link item.

    Args:
        response (ApiResponseFactory): Factory response object
        link_id (str): ID of the Link item to get metrics for

    Returns:
        response: The response for this request from the actor
    """
    response.set_response_from_actor(
        actor,
        GetTotalClicksUseCase(link_id),
        'link'
    )
    return response


@handler(AwsApiGatewayResponseFactory(return_trace=True), arg_mappers=[get_aws_lambda_path_param_arg_mapper('id'), get_aws_lambda_path_param_arg_mapper('date')])
def get_total_clicks_for_link_by_day(response: ApiResponseFactory, link_id: str, date: str) -> ApiResponseFactory:
    """Return the total number of clicks for a Link item on a given day.

    Args:
        response (ApiResponseFactory): Factory response object
        link_id (str): ID of the Link item to get metrics for
        date (str): ISO-formatted date string to check for

    Returns:
        response: The response for this request from the actor
    """
    response.set_response_from_actor(
        actor,
        GetTotalClicksByDayUseCase(link_id, date),
        'link'
    )
    return response


@handler(AwsApiGatewayResponseFactory(return_trace=True), arg_mappers=[get_aws_lambda_path_param_arg_mapper('id'), get_aws_lambda_path_param_arg_mapper('ip')])
def get_total_clicks_for_link_by_person(response: ApiResponseFactory, link_id: str, person_id: str) -> ApiResponseFactory:
    """Return the total number of clicks for a Link item by a given person.

    Args:
        response (ApiResponseFactory): Factory response object
        link_id (str): ID of the Link item to get metrics for
        person_id (str): ID of the Person for click metrics(currently an IP address)

    Returns:
        response: The response for this request from the actor
    """
    rehydrated_ip = person_id.replace('-', '.')
    response.set_response_from_actor(
        actor,
        GetTotalClicksByPersonUseCase(link_id, rehydrated_ip),
        'link'
    )
    return response


@handler(AwsApiGatewayResponseFactory(return_trace=True), arg_mappers=[get_aws_lambda_path_param_arg_mapper('id'),
                                                                       get_aws_lambda_path_param_arg_mapper('ip'),
                                                                       get_aws_lambda_path_param_arg_mapper('date')])
def get_total_clicks_for_link_by_person_by_day(response: ApiResponseFactory, link_id: str, person_id: str, date: str) -> ApiResponseFactory:
    """Return the total number of clicks for a Link item by a given person on a given day.

    Args:
        response (ApiResponseFactory): Factory response object
        link_id (str): ID of the Link item to get metrics for
        person_id (str): ID of the Person for click metrics(currently an IP address)
        date (str): ISO-formatted date string to check for

    Returns:
        response: The response for this request from the actor
    """
    rehydrated_ip = person_id.replace('-', '.')
    response.set_response_from_actor(
        actor,
        GetTotalPersonClicksByDayUseCase(link_id, rehydrated_ip, date),
        'link'
    )
    return response
