"""This script contains the Link Repository, which is backed by DynamoDB. It also contains some exception classes used
to catch and deal with certain errors from the database, as examples."""
from typing import List

from ckf_api_toolkit.aws_dynamo import DynamoRepository, DynamoRequestNotFound, DynamoInstruction, DynamoActionType, \
    DynamoAttributesResponse, DynamoItemsResponse
from ckf_api_toolkit.aws_dynamo.overloaded_gsi.composite_key_utils import KEY_DELIMITER
from ckf_api_toolkit.aws_dynamo.overloaded_gsi.payload_factory import OverloadedGsiPayloadFactory
from ckf_api_toolkit.tools.api_response_handler import ApiResponseException, ResponseCode
from ckf_api_toolkit.aws_dynamo import DynamoConditionCheckFailed

from link_shortener_constants import OverloadedGsiKeys
from models.link import Link
from models.person_click import PersonClick
from use_cases import CreateLinkUseCase, UpdateLinkUseCase, GetActualLinkFromShortLinkUseCase, CreatePersonClickUseCase, \
    GetTotalClicksUseCase, GetTotalClicksByDayUseCase, GetTotalClicksByPersonUseCase, GetTotalPersonClicksByDayUseCase, \
    DeleteLinkUseCase


class NotFound(ApiResponseException):
    """An ApiResponseException to change the error message for "item not found"(i.e. HTTP 404)"""
    def __init__(self):
        super(NotFound, self).__init__(ResponseCode.NOT_FOUND, "This item doesn't exist!")


class AlreadyExists(Exception):
    """A simple wrapper around "item already exists" in order to be caught in the retry logic of the create handler."""
    pass


class LinkShortenerRepository(DynamoRepository):
    """A DynamoRepository representing a datastore for Link items"""
    payload_factory: OverloadedGsiPayloadFactory

    def __init__(self):
        """Initialize the repo with any error conversions needed and the payload factory, and add all UseCases by
        mapping them to their repository method."""
        super(LinkShortenerRepository, self).__init__(table_name='ShortLinkTable')
        self.add_error_conversion(DynamoRequestNotFound, NotFound)
        self.add_error_conversion(DynamoConditionCheckFailed, AlreadyExists)
        self.payload_factory = OverloadedGsiPayloadFactory(
            table_name=self.table_name,
            pk_name=OverloadedGsiKeys.PK.value,
            sk_name=OverloadedGsiKeys.SK.value,
            gsi_key_prefix=OverloadedGsiKeys.GSI_PREFIX.value,
        )

        # Links
        self.add_use_case(CreateLinkUseCase, self.create_link)
        self.add_use_case(UpdateLinkUseCase, self.update_link)
        self.add_use_case(GetActualLinkFromShortLinkUseCase, self.get_actual_by_short)

        # PersonClicks
        self.add_use_case(CreatePersonClickUseCase, self.create_person_click)
        self.add_use_case(GetTotalClicksUseCase, self.get_total_clicks_for_link)
        self.add_use_case(GetTotalClicksByDayUseCase, self.get_total_clicks_for_link_by_day)
        self.add_use_case(GetTotalClicksByPersonUseCase, self.get_total_clicks_for_link_by_person)
        self.add_use_case(GetTotalPersonClicksByDayUseCase, self.get_total_clicks_for_link_by_person_by_day)

    # Links
    def create_link(self, use_case: CreateLinkUseCase) -> DynamoInstruction:
        """Wraps CreateLinkUseCase. Note that despite the DynamoActionType being UPDATE(as this simplifies some things
        in CKF), the create use case needs its own method due to the payload factory method requiring enforce_unique to
        be set.

        Args:
            use_case (CreateLinkUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the UPDATE action with the payload created for this use case
        """
        def __parser(dynamo_response: DynamoAttributesResponse) -> dict:
            return self._update_model_item_parser(dynamo_response, Link, private=True)

        payload = self.payload_factory.get_update_payload(use_case.link, enforce_unique=True)
        return DynamoInstruction(DynamoActionType.UPDATE, payload, __parser)

    def update_link(self, use_case: UpdateLinkUseCase) -> DynamoInstruction:
        """Wraps UpdateLinkUseCase.

        Args:
            use_case (UpdateLinkUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the UPDATE action with the payload created for this use case
        """
        def __parser(dynamo_response: DynamoAttributesResponse) -> dict:
            return self._update_model_item_parser(dynamo_response, Link, private=True)

        payload = self.payload_factory.get_update_payload(use_case.link)
        return DynamoInstruction(DynamoActionType.UPDATE, payload, __parser)

    def get_actual_by_short(self, use_case: GetActualLinkFromShortLinkUseCase) -> DynamoInstruction:
        """Wraps GetActualLinkFromShortLinkUseCase.

        Args:
            use_case (GetActualLinkFromShortLinkUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the QUERY action with the payload created for this use case
        """
        def __parser(dynamo_response: List[DynamoItemsResponse]) -> dict:
            return self._query_single_model_item_parser(dynamo_response, Link, as_instance=True)
        payload = self.payload_factory.get_explicit_pk_query_payload(use_case.link_id)
        return DynamoInstruction(DynamoActionType.QUERY, payload, __parser)

    def delete_link(self, use_case: DeleteLinkUseCase) -> DynamoInstruction:
        """Wraps DeleteLinkUseCase.

        Args:
            use_case (GetActualLinkFromShortLinkUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the DELETE action with the payload created for this use case
        """
        payload = self.payload_factory.get_composite_key_delete_payload([use_case.link_id])
        return DynamoInstruction(DynamoActionType.DELETE, payload, lambda _: None)

    # PersonClicks
    def create_person_click(self, use_case: CreatePersonClickUseCase) -> DynamoInstruction:
        """Wraps CreatePersonClickUseCase.

        Args:
            use_case (CreatePersonClickUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the UPDATE action with the payload created for this use case
        """
        def __parser(dynamo_response: DynamoAttributesResponse) -> dict:
            return self._update_model_item_parser(dynamo_response, PersonClick)

        payload = self.payload_factory.get_update_payload(use_case.person_click)
        return DynamoInstruction(DynamoActionType.UPDATE, payload, __parser)

    def get_total_clicks_for_link(self, use_case: GetTotalClicksUseCase) -> DynamoInstruction:
        """Wraps GetTotalClicksUseCase.

        Args:
            use_case (GetTotalClicksUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the QUERY action with the payload created for this use case
        """
        def __parser(dynamo_response: List[DynamoItemsResponse]) -> int:
            response = self._query_model_item_list_parser(dynamo_response, PersonClick)
            return len(response)

        payload = self.payload_factory.get_sk_begins_with_payload(PersonClick.model, use_case.link_id, gsi=1)
        return DynamoInstruction(DynamoActionType.QUERY, payload, __parser)

    def get_total_clicks_for_link_by_day(self, use_case: GetTotalClicksByDayUseCase) -> DynamoInstruction:
        """Wraps GetTotalClicksByDayUseCase.

        Args:
            use_case (GetTotalClicksByDayUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the QUERY action with the payload created for this use case
        """
        def __parser(dynamo_response: List[DynamoItemsResponse]) -> int:
            response = self._query_model_item_list_parser(dynamo_response, PersonClick)
            return len(response)

        payload = self.payload_factory.get_sk_begins_with_payload(PersonClick.model,
                                                                  f"{use_case.link_id}{KEY_DELIMITER}{use_case.date}",
                                                                  gsi=1)
        return DynamoInstruction(DynamoActionType.QUERY, payload, __parser)

    def get_total_clicks_for_link_by_person(self, use_case: GetTotalClicksByPersonUseCase) -> DynamoInstruction:
        """Wraps GetTotalClicksByPersonUseCase.

        Args:
            use_case (GetTotalClicksByPersonUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the QUERY action with the payload created for this use case
        """
        def __parser(dynamo_response: List[DynamoItemsResponse]) -> int:
            response = self._query_model_item_list_parser(dynamo_response, PersonClick)
            return len(response)

        payload = self.payload_factory.get_sk_begins_with_payload(use_case.person_id,
                                                                  f"{use_case.link_id}",
                                                                  gsi=2)
        return DynamoInstruction(DynamoActionType.QUERY, payload, __parser)

    def get_total_clicks_for_link_by_person_by_day(self, use_case: GetTotalPersonClicksByDayUseCase) -> DynamoInstruction:
        """Wraps GetTotalPersonClicksByDayUseCase.

        Args:
            use_case (GetTotalPersonClicksByDayUseCase): The use case to run

        Returns:
            A DynamoInstruction to run the QUERY action with the payload created for this use case
        """
        def __parser(dynamo_response: List[DynamoItemsResponse]) -> int:
            response = self._query_model_item_list_parser(dynamo_response, PersonClick)
            return len(response)

        payload = self.payload_factory.get_sk_begins_with_payload(use_case.person_id,
                                                                  f"{use_case.link_id}{KEY_DELIMITER}{use_case.date}",
                                                                  gsi=2)
        return DynamoInstruction(DynamoActionType.QUERY, payload, __parser)
